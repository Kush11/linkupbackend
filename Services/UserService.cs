﻿using LinkUp.Context;
using LinkUp.Helpers;
using LinkUp.Models;
using Microsoft.Extensions.Options;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;
using LinkUp.Models.DTOs;

namespace LinkUp.Services
{
    public interface IUserService
    {
        Users Authenticate(string username, string password);
        Task <IEnumerable<Users>> GetAll();
        Task <Users> GetById(Guid id);

        Task<Users> GetByEmail(string email);

        Task Create(Users user, string password);
        Task Update(Users user, string password = null);
        Task Delete(Guid id);
    }

    public class UserService : IUserService
    {
        private readonly RideMatchContext _context;
        private readonly AppSettings _appSettings;
        private readonly IConfiguration configuration;



        public UserService(IOptions<AppSettings> appSettings, IConfiguration iConfig, RideMatchContext context)
        {
            _appSettings = appSettings.Value;
            _context = context;
            configuration = iConfig;

        }


        public Users Authenticate(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                return null;

            var user = _context.Users.SingleOrDefault(x => x.UserName == username || x.Email == username && x.Password == password);

            //check if the username exists
            if (user == null)
                return null;

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.Default.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
               {
                     new Claim(ClaimTypes.NameIdentifier, user.UserId.ToString()),
                     //new Claim(ClaimTypes.Email, user.Email),
                     new Claim(ClaimTypes.Role, user.Role)
               }),
                Expires = DateTime.UtcNow.AddDays(365),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha512Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = tokenHandler.WriteToken(token);

            //check if password is correct
            if (!VerifyPasswordHash(password, user.PasswordHash, user.PasswordSalt))
                return null;

            //authenticate sucessfull
            return user;
        }


        public async Task<IEnumerable<Users>> GetAll()
        {
            return await _context.Users
                .Include(up => up.UserPaymentData)
                .ToListAsync();

        }


        public async Task <Users> GetById(Guid id)
        {
            //return _context.Users.Find(id);
            var user = await _context.Users
                .Include(up => up.UserPaymentData)
                .Include(nt => nt.PushNotificationTokens)
                .SingleOrDefaultAsync(x => x.UserId == id);

            //return user without password
            if (user != null)
                user.Password = null;

            return user;
        }

        public async Task<Users> GetByEmail(string email)
        {
            var user = await _context.Users
            .SingleOrDefaultAsync(x => x.Email == email);
            return user;
        }


        public async Task Create(Users user, string password)
        {
            //validation
            if (string.IsNullOrWhiteSpace(password))
                throw new AppException("Password is required");

            if (await _context.Users.AnyAsync(x => x.Email == user.Email))
               throw new AppException("Email \"" + user.Email + "\" already exists.");

          
            CreatePasswordHash(password, out byte[] passwordHash, out byte[] passwordSalt);

            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;
            SendVerificationToken(user.VerificationCode, user.PhoneNumber);

           await  _context.Users.AddAsync(user);
            await _context.SaveChangesAsync();

            //_userManager.AddClaimAsync(user, new Claim(ClaimTypes.Email, user.Email));

            return;
        }

        public async Task Update(Users userParam, string password = null)
        {
            var user = await _context.Users.FindAsync(userParam.UserId);

            if (user == null)
                throw new AppException("User not found");

            if (userParam.Email != user.Email)
            {
                // username has changed so check if the new username is already taken
                if (await _context.Users.AnyAsync(x => x.Email == userParam.Email))
                    throw new AppException("This account already exists");
            }

            // update user properties
            user.UserStatus = userParam.UserStatus;
            user.UserName = userParam.UserName;
            user.PhoneNumber = userParam.PhoneNumber;
            user.Token = userParam.Token;
            user.Role = userParam.Role;
            user.ImageUrl = userParam.ImageUrl;


            // update password if it was entered
            if (!string.IsNullOrWhiteSpace(password))
            {
                CreatePasswordHash(password, out byte[] passwordHash, out byte[] passwordSalt);

                user.PasswordHash = passwordHash;
                user.PasswordSalt = passwordSalt;
            }

             _context.Users.Update(user);
            await _context.SaveChangesAsync();
        }

        public async Task Delete(Guid id)
        {
            var user = await _context.Users.FindAsync(id);
            if (user != null)
            {
                _context.Users.Remove(user);
                await _context.SaveChangesAsync();
            }
        }

        // private helper methods
        private static void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");

            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        private static bool VerifyPasswordHash(string password, byte[] storedHash, byte[] storedSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");
            if (storedHash.Length != 64) throw new ArgumentException("Invalid length of password hash (64 bytes expected).", "passwordHash");
            if (storedSalt.Length != 128) throw new ArgumentException("Invalid length of password salt (128 bytes expected).", "passwordHash");

            using (var hmac = new System.Security.Cryptography.HMACSHA512(storedSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != storedHash[i]) return false;
                }
            }

            return true;
        }

        public void SendVerificationToken(string verifyCode, string deliveryNumber)
        {
            string accountSid = configuration.GetValue<string>("TwilioConfig:AccountSid");
            string authToken = configuration.GetValue<string>("TwilioConfig:AuthToken");

            TwilioClient.Init(accountSid, authToken);

            var message = MessageResource.Create(
                body: verifyCode,
                from: new Twilio.Types.PhoneNumber("+19382229458"),
                to: new Twilio.Types.PhoneNumber(deliveryNumber)
            );

            Console.WriteLine(message.Sid);
        }
    }

 

}

﻿using CorePush.Google;
using Lib.Net.Http.WebPush;
using LinkUp.Context;
using LinkUp.Helpers;
using LinkUp.Models;
using LiteDB;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebPush;
using PushSubscription = WebPush.PushSubscription;

namespace LinkUp.Services
{

    public interface IPushSubscriptionStore<TEntity>
    {
        PushNotificationToken StoreSubscriptionAsync(PushNotificationToken subscription);
        void Delete(PushNotificationToken sub);
        Task SendNotification(NotificationMessage notification);
        Task<PushNotificationToken> GetById(Guid id);
        Task Update(TEntity dbEntity, TEntity entity);

        //Task Update(PushNotificationToken token);

    }


    public class PushSubscriptionService : IPushSubscriptionStore<PushNotificationToken>
    {

        private readonly RideMatchContext _context;
        private readonly string _serverKey;
        public PushSubscriptionService(IConfiguration configuration, RideMatchContext context)
        {
            _context = context;
            _serverKey = configuration.GetSection("FCM:ServerKey").Value;
        }

        public async void Delete(PushNotificationToken sub)
        {
            var item = _context.PushNotificationToken.FirstOrDefault(s => s.TokenId == sub.TokenId);
            if(item != null)
            {
                _context.PushNotificationToken.Remove(item);
                await _context.SaveChangesAsync();
            }
        }

      

        public async Task SendNotification(NotificationMessage notification)
        {
           
            var user = notification.Notification.ReceiverName;
            var token = notification.To;
            var fcm = new FcmSender(_serverKey, user);
            await fcm.SendAsync(token,
                new
                {
                    notification = new
                    {
                        title = notification.Notification.Title,
                        body = notification.Notification.Body,
                        click_action = notification.Notification.ClickAction
                    },
                    data = notification.Data
                });

        }

        public PushNotificationToken StoreSubscriptionAsync(PushNotificationToken subscription)
        {
            //if (_context.Subscriptions.Any(x => x.Endpoint == subscription.Endpoint))
            //    throw new AppException("subscription \"" + subscription.Endpoint + "\" already exists.");

            _context.PushNotificationToken.Add(subscription);
            _context.SaveChangesAsync();
            return subscription;
        }

        public async Task <PushNotificationToken> GetById(Guid id)
        {
            var sub = await _context.PushNotificationToken
                .SingleOrDefaultAsync(u => u.UserId == id);
            return sub;
        }

        public async Task Update(PushNotificationToken pushToken, PushNotificationToken entity)
        {
            pushToken.Token = entity.Token;
            await _context.SaveChangesAsync();
        }
    }
}

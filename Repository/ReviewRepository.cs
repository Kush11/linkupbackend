﻿using LinkUp.Context;
using LinkUp.Models;
using LinkUp.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LinkUp.Repository
{
    public class ReviewRepository : _dataRepository<AppReview>
    {

        readonly RideMatchContext _context;

        public ReviewRepository(RideMatchContext context)
        {
            _context = context;
        }

        public async Task Add(AppReview review)
        {
            await _context.AppReview.AddAsync(review);
            await _context.SaveChangesAsync();
        }

        public async Task Delete(Guid reviewId)
        {
            var review =await  _context.AppReview.FindAsync(reviewId);
            if (review != null)
            {
                _context.AppReview.Remove(review);
               await  _context.SaveChangesAsync();
            }

        }

        public async Task<AppReview>  Get(Guid id)
        {
            return await _context.AppReview
                .SingleOrDefaultAsync(c => c.UserId == id);
        }

        public async Task<IEnumerable<AppReview>>  GetAll()
        {
            return  await _context.AppReview
                .ToListAsync();
        }

        public async Task Update(AppReview review, AppReview entity)
        {
            review.UserReview = entity.UserReview;
            review.UserRating = entity.UserRating;
           await _context.SaveChangesAsync();

        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LinkUp.Repository.Interface
{
    public interface _imageRepository<TEntity>
    {
       Task <IEnumerable<TEntity>> GetAll();
        Task <TEntity> Get(Guid id);
        Task Add(TEntity entity);
        Task Update(TEntity dbEntity, TEntity entity);
        Task Delete(TEntity entity);
    }
}

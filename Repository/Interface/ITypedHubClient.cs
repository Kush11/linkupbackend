﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LinkUp.Repository.Interface
{
    public interface ITypedHubClient
    {
        Task ReceiveMessage(string message);
        Task GetConnectionId(string id);
        Task ReceiveDeclineMessage(string message);
        Task AddToGroup(string groupName);
        Task ReceiveGroupMessage(string message);
        Task SendGroupMessage(string message);
    }
}

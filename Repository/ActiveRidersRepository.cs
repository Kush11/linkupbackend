﻿using LinkUp.Context;
using LinkUp.Models;
using LinkUp.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LinkUp.Repository
{
    public class ActiveRidersRepository : _dataRepository<ActiveRiders>
    {
        readonly RideMatchContext _context;

        public ActiveRidersRepository(RideMatchContext context)
        {
            _context = context;
        }

        public async Task Add(ActiveRiders trips)
        {
           await  _context.ActiveRiders.AddAsync(trips);
           await _context.SaveChangesAsync();
        }

        public async Task Delete(Guid riderId)
        {
            var rider = await _context.ActiveRiders.FindAsync(riderId);
            if (rider != null)
            {
                 _context.ActiveRiders.Remove(rider);
               await  _context.SaveChangesAsync();
            }
           
        }

        public async Task<ActiveRiders> Get(Guid id)
        {
            return await _context.ActiveRiders
                .Include(u => u.User)
                .Include(t => t.Trip)
                .SingleOrDefaultAsync(c => c.ActiveRiderId == id);
        }

        public async Task<IEnumerable<ActiveRiders>> GetAll()
        {
            return await _context.ActiveRiders
                .Include(u => u.User)
                .Include(t => t.Trip)
                .ToListAsync();
        }

        public async Task Update(ActiveRiders trips, ActiveRiders entity)
        {
            trips.TripStatus = entity.TripStatus;
            trips.PaymentStatus = entity.PaymentStatus;
            trips.RiderConnectId = entity.RiderConnectId;
            await _context.SaveChangesAsync();
        }
    }
}

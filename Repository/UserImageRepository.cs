﻿using LinkUp.Context;
using LinkUp.Models;
using LinkUp.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LinkUp.Repository
{
    public class UserImageRepository : _imageRepository<UserImage>
    {
        readonly RideMatchContext _context;
        
        public UserImageRepository(RideMatchContext context)
        {
            _context = context;
        }

         public async Task Add(UserImage entity)
        {
           await  _context.Image.AddAsync(entity);
           await _context.SaveChangesAsync();
        }

        public async Task Delete(UserImage image)
        {
            _context.Image.Remove(image);
           await _context.SaveChangesAsync();
        }

        public async Task<UserImage>  Get(Guid id)
        {
            return await _context.Image
                .SingleOrDefaultAsync(c => c.UserId == id);
        }

        public async Task<IEnumerable<UserImage>> GetAll()
        {
            return await _context.Image
                .ToListAsync();
        }

        public async Task Update(UserImage images, UserImage entity)
        {
            images.Image = entity.Image;
            await _context.SaveChangesAsync();
        }
    }
    
}

﻿using LinkUp.Context;
using LinkUp.Models;
using LinkUp.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LinkUp.Repository
{
    public class PaymentDataRepository : _dataRepository<UserPaymentData>
    {
        readonly RideMatchContext _context;

        public PaymentDataRepository(RideMatchContext context)
        {
            _context = context;
        }

        public async Task Add(UserPaymentData payment)
        {
            await _context.PaymentData.AddAsync(payment);
            await _context.SaveChangesAsync();
        }

        public async Task Delete(Guid paymentId)
        {
            var payment = await _context.PaymentData.FindAsync(paymentId);
            if (payment != null)
            {
                _context.PaymentData.Remove(payment);
                await _context.SaveChangesAsync();
            }

        }

        public async Task<UserPaymentData> Get(Guid id)
        {
            return await _context.PaymentData
                .SingleOrDefaultAsync(c => c.PaymentId == id);
        }

        public async Task<IEnumerable<UserPaymentData>> GetAll()
        {
            return await _context.PaymentData
                .ToListAsync();
        }

        public async Task Update(UserPaymentData payment, UserPaymentData entity)
        {
            payment.PaymentToken = entity.PaymentToken;
            await _context.SaveChangesAsync();

        }


    }
}

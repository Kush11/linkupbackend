﻿using LinkUp.Context;
using LinkUp.Models;
using LinkUp.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LinkUp.Repository
{
    public class PickUpRepository : _dataRepository<TripPickUp>
    {
        readonly RideMatchContext _context;

        public PickUpRepository(RideMatchContext context)
        {
            _context = context;
        }

        public async Task Add(TripPickUp trips)
        {
            await _context.TripPickUps.AddAsync(trips);
            await _context.SaveChangesAsync();
        }

        public async Task Delete(Guid id)
        {
            var pickup = await  _context.TripPickUps.FindAsync(id);
            if (pickup != null)
            {
                _context.TripPickUps.Remove(pickup);
                await _context.SaveChangesAsync();
            }

        }

        public async Task<TripPickUp>  Get(Guid id)
        {
            return await _context.TripPickUps
                .FirstOrDefaultAsync(c => c.UserId == id);
        }

        public async Task <IEnumerable<TripPickUp>> GetAll()
        {
            return await _context.TripPickUps
                .ToListAsync();
        }

        public async Task Update(TripPickUp trips, TripPickUp entity)
        {
            trips.PickupLongitude = entity.PickupLongitude;
            trips.PickupLatitude = entity.PickupLatitude;
            await _context.SaveChangesAsync();

        }
    }
}



﻿using LinkUp.Context;
using LinkUp.Models;
using LinkUp.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LinkUp.Repository
{
    public class DriverLicenseRepository: _imageRepository<DriverLicense>
    {
        readonly RideMatchContext _context;

        public DriverLicenseRepository(RideMatchContext context)
        {
            _context = context;
        }

        public async Task Add(DriverLicense entity)
        {
            await _context.License.AddAsync(entity);
            await _context.SaveChangesAsync();
        }

        public async Task Delete(DriverLicense image)
        {
            _context.License.Remove(image);
            await _context.SaveChangesAsync();
        }

        public async Task<DriverLicense> Get(Guid id)
        {
            return await _context.License
            .SingleOrDefaultAsync(c => c.DriverId == id);
        }

        public async Task<IEnumerable<DriverLicense>> GetAll()
        {
            return await _context.License
                .ToListAsync();
        }

        public async Task Update(DriverLicense images, DriverLicense entity)
        {
            images.CarLicense = entity.CarLicense;
            await _context.SaveChangesAsync();
        }
    }
}

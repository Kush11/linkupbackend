﻿using LinkUp.Context;
using LinkUp.Models;
using LinkUp.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LinkUp.Repository
{
    public class DriverDataRepository : _dataRepository<DriverData>
    {

        readonly RideMatchContext _context;

        public DriverDataRepository(RideMatchContext context)
        {
            _context = context;
        }


        public async Task Add(DriverData driver)
        {
           await  _context.DriverData.AddAsync(driver);
            await _context.SaveChangesAsync();
        }

        public async Task Delete(Guid driverId)
        {
            var driver =await _context.DriverData.FindAsync(driverId);
            if(driver != null)
            {
                _context.DriverData.Remove(driver);
                await _context.SaveChangesAsync();
            }
        
        }

        public async Task<DriverData>  Get(Guid id)
        {
            return await _context.DriverData
                              .Include(u => u.Driver)

                .SingleOrDefaultAsync(c => c.DriverId == id);
        }

        public async Task<IEnumerable<DriverData>> GetAll()
        {
            return await _context.DriverData
               .Include(u => u.Driver)
               .ToListAsync();
        }

        public async Task Update(DriverData driver, DriverData entity)
        {
            driver.CarType = entity.CarType;
            driver.WorkAddress = entity.WorkAddress;
            driver.LicenseUrl = entity.LicenseUrl;
            driver.PlateNumber = entity.PlateNumber;
            driver.RideDeclineCount = entity.RideDeclineCount;
            driver.DriverStatus = entity.DriverStatus;
            await _context.SaveChangesAsync();

        }
    }
}

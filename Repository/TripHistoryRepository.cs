﻿using LinkUp.Context;
using LinkUp.Models;
using LinkUp.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LinkUp.Repository
{
    public class TripHistoryRepository: _dataRepository<Trips>
    {
        readonly RideMatchContext _context;

        public TripHistoryRepository(RideMatchContext context)
        {
            _context = context;
        }

        public async Task Add(Trips trips)
        {
            await _context.Trips.AddAsync(trips);
            await _context.SaveChangesAsync();
        }

        public async Task Delete(Guid tripId)
        {
            var trip = await _context.Trips.FindAsync(tripId);
            if (trip != null)
            {
                _context.Trips.Remove(trip);
                await _context.SaveChangesAsync();
            }

        }

        public async Task<Trips> Get(Guid id)
        {
            return await _context.Trips
                .Include(ar => ar.ActiveRiders)
                .ThenInclude(us => us.User)
                .Include(td => td.TripDriver)
                .ThenInclude(u => u.Driver)
                .SingleOrDefaultAsync(c => c.TripId == id);
        }

        public async Task<IEnumerable<Trips>> GetAll()
        {
            return await _context.Trips
                .Include(ar => ar.ActiveRiders)
                .ThenInclude(us => us.User)
                .Include(td => td.TripDriver)
                .ThenInclude(u => u.Driver)
                .ToListAsync();
        }

        public async Task Update(Trips trips, Trips entity)
        {
            trips.DriverTripStatus = entity.DriverTripStatus;
            trips.AllowedRiderCount = entity.AllowedRiderCount;
            trips.TripStartDateTime = entity.TripStartDateTime;
            trips.TripEndDateTime = entity.TripEndDateTime;
            trips.ActualTripStartDateTime = entity.ActualTripStartDateTime;
            trips.ActualTripEndDateTime = entity.ActualTripEndDateTime;
            await _context.SaveChangesAsync();

        }

    }
}

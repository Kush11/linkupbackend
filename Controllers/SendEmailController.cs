﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinkUp.Models;
using LinkUp.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LinkUp.Controllers
{
    [Route("api/sendmail")]
    [ApiController]
    public class SendEmailController : ControllerBase
    {
        private readonly IEmailService _emailService;


        public SendEmailController(IEmailService email)
        {
            _emailService = email;
        }

        // POST api/auth
        [AllowAnonymous]
        [HttpPost]
        public IActionResult SendMail([FromBody] EmailMessage mail)
        {
            try
            {
                 _emailService.Send(mail);
                return Ok();
            }
            catch (Exception)
            {
                return BadRequest();
            }


        }
    }
}
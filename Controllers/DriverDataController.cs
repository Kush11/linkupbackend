﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using LinkUp.Models;
using LinkUp.Repository.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LinkUp.Controllers
{
    [Authorize]
    [Route("api/driverdata")]
    [ApiController]
    public class DriverDataController : ControllerBase
    {
        private readonly _dataRepository<DriverData> _dataRepository;
        public DriverDataController(_dataRepository<DriverData> dataRepository)
        {
            _dataRepository = dataRepository;
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            IEnumerable<DriverData> data = await _dataRepository.GetAll();
            return Ok(data);

        }

        // GET api/driverdata/5
        [Authorize(Roles = "Admin, Rider, Driver")]
        [HttpGet("{id}", Name = "GetDriverData")]
        public async Task<IActionResult> Get(Guid id)
        {
            DriverData data = await _dataRepository.Get(id);
            if (data == null)
            {
                return NotFound("The Driver record couldn't be found.");
            }
            return Ok(data);
        }
      


        [Authorize(Roles = "Admin, Driver")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] DriverData data)
        {
            if (data == null)
            {
                return BadRequest("Driver data request is null");
            }
           await _dataRepository.Add(data);
            return CreatedAtRoute(
                    "GetDriverData",
                    new { Id = data.DriverDataId },
                    data);
        }

   

        //PUT: api/driverdata/5
        [Authorize(Roles = "Admin, Driver")]
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, [FromBody] DriverData data)
        {
            if (data == null)
            {
                return BadRequest("Driver data is null ");
            }

            DriverData dataToUpdate = await _dataRepository.Get(id);
            if (dataToUpdate == null)
            {
                return NotFound("The data could not be found");
            }

            await _dataRepository.Update(dataToUpdate, data);
            return NoContent();
        }

        //Delete: api/stores/5
        [Authorize(Roles = "Admin")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            await _dataRepository.Delete(id);
            return Ok();
        }

    }
}
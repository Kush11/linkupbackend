﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using LinkUp.Context;
using LinkUp.Models;
using LinkUp.Models.DTOs;
using LinkUp.Repository.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LinkUp.Controllers
{
    [Authorize]
    [Route("api/ActiveTrip")]
    [ApiController]
    public class ActiveTripController : ControllerBase
    {
        private readonly _dataRepository<Trips> _dataRepository;
        private readonly RideMatchContext _context;
        private readonly IMapper _mapper;

        public ActiveTripController(_dataRepository<Trips> dataRepository, RideMatchContext context, IMapper mapper)
        {
            _dataRepository = dataRepository;
            _context = context;
            _mapper = mapper;
        }

        // GET: api/stores

        [Authorize(Roles = "Admin, Rider, Driver")]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            IEnumerable<Trips> trips = await _dataRepository.GetAll();
            return Ok(trips);

        }

        [Authorize(Roles = "Admin, Rider, Driver")]
        [Route("history")]
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            IEnumerable<Trips> trips = await _context.Trips
                .Include(ar => ar.ActiveRiders)
                .ThenInclude(us => us.User)
                .Include(td => td.TripDriver)
                .ThenInclude(u => u.Driver)
                .ToListAsync();
            if(trips == null)
            {
                return NotFound("The trip record couldn't be found.");

            }
            return Ok(trips);

        }

        // GET api/Activetrips/5
        [Authorize(Roles = "Admin, Driver, Rider")]
        [HttpGet("{id}", Name = "GetTrips")]
        public async Task<IActionResult> Get(Guid id)
        {
            Trips trips = await _dataRepository.Get(id);
            if (trips == null)
            {
                return NotFound("The trip record couldn't be found.");
            }
            return Ok(trips);
        }
      
        [Authorize(Roles = "Admin, Driver ")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] TripDTO tripsDto)
        {
            var trips = _mapper.Map<Trips>(tripsDto);
            if (trips == null)
            {
                return BadRequest("trip is null");
            }
           await _dataRepository.Add(trips);
            return CreatedAtRoute(
                    "GetTrips",
                    new { Id = trips.TripId },
                    trips);
        }

        //[Route("send-email")]
        //public async Task<IActionResult> SendEmailAsync([FromQuery] string username, string subject, string message)
        //{
        //    await _emailService.SendEmailAsync(username, subject, message);
        //    return Ok();
        //}

        [Authorize(Roles = "Admin, Driver")]
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, [FromBody] Trips trips)
        {
            if (trips == null)
            {
                return BadRequest("trips is null ");
            }

            Trips tripToUpdate = await _dataRepository.Get(id);
            if (tripToUpdate == null)
            {
                return NotFound("The trip could not be found");
            }

           await _dataRepository.Update(tripToUpdate, trips);
            return NoContent();
        }

        //Delete: api/stores/5
        [Authorize(Roles = "Admin")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            await _dataRepository.Delete(id);
            return Ok();
        }
    }
}
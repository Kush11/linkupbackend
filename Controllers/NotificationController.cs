﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lib.Net.Http.WebPush;
using LinkUp.Helpers;
using LinkUp.Models;
using LinkUp.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using WebPush;
using PushSubscription = WebPush.PushSubscription;


namespace LinkUp.Controllers
{
    [Route("api/notification")]
    [ApiController]
    public class NotificationController : ControllerBase
    {
        private readonly IPushSubscriptionStore<PushNotificationToken> _subscriptionStore;
        private readonly VapidConfig _options;
        public static List<PushSubscription> Subscriptions { get; set; } = new List<PushSubscription>();



        public NotificationController(IPushSubscriptionStore<PushNotificationToken> subscriptionStore)
        {
            _subscriptionStore = subscriptionStore;
        }

        [HttpPost]
        public  IActionResult StoreSubscription([FromBody]PushNotificationToken subscription)
        {
             _subscriptionStore.StoreSubscriptionAsync(subscription);


            return Ok();
        }

        [HttpPost("unsubscribe")]
        public IActionResult Unsubscribe([FromBody]PushNotificationToken subscription)
        {
            _subscriptionStore.Delete(subscription);
            return Ok();
        }

        [HttpPost("send")]
        public async Task Send (NotificationMessage notification)
        {
            await _subscriptionStore.SendNotification(notification);
        }

        [HttpGet]
        public ContentResult Get()
        {
            return Content(_options.PublicKey, "text/plain");
        }

        [HttpGet("{id}", Name = "GetNotificationSub")]
        public async Task<IActionResult> Get(Guid id)
        {
            PushNotificationToken token = await _subscriptionStore.GetById(id);
            if (token == null)
            {
                return NotFound("Token not found.");
            }

            return Ok(token);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(Guid id, [FromBody] PushNotificationToken token)
        {
            if (token == null)
            {
                return BadRequest("token is null ");
            }

            PushNotificationToken tokenToUpdate = await _subscriptionStore.GetById(id);

            if(tokenToUpdate == null)
            {
                return NotFound("Token not found");
            }
            await _subscriptionStore.Update(tokenToUpdate, token);
            return NoContent();
        
        }
    }
}
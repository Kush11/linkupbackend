﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinkUp.Context;
using LinkUp.Models;
using LinkUp.Repository.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LinkUp.Controllers
{
    [Route("/api/user/image")]
    [ApiController]
    public class UserImageController : ControllerBase
    {
        private readonly _imageRepository<UserImage> _dataRepository;

        public UserImageController(_imageRepository<UserImage> dataRepository)
        {
            _dataRepository = dataRepository;
        }

        [Authorize(Roles = "Admin, Driver, Rider")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]UserImage filesData)
        {
            if (filesData == null) return BadRequest("Null File");

            await _dataRepository.Add(filesData);
            return CreatedAtRoute(
                 "GetImage",
                    new { Id = filesData.ImageId },
                    filesData);
        }

        [Authorize(Roles = "Admin, Driver, Rider")]
        [HttpGet]
        public async Task<IActionResult> Getimage()
        {

            IEnumerable<UserImage> files = await _dataRepository.GetAll();
            if (files == null)
            {
                return NotFound("The image couldn't be found");
            }
            return Ok(files);

        }

        [Authorize(Roles = "Admin, Driver, Rider")]
        [HttpGet("{id}", Name = "GetImage")]
        public async Task<IActionResult> Get(Guid id)
        {
            UserImage images = await _dataRepository.Get(id);
            if (images == null)
            {
                return NotFound("The image couldn't be found.");
            }
            return Ok(images);
        }

        [Authorize(Roles = "Admin, Driver, Rider")]
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, [FromBody] UserImage image)
        {
            if (image == null)
            {
                return BadRequest("image is null ");
            }

            UserImage imageToUpdate = await _dataRepository.Get(id);
            if (imageToUpdate == null)
            {
                return NotFound("The image could not be found");
            }

            await _dataRepository.Update(imageToUpdate, image);
            return NoContent();
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using LinkUp.Context;
using LinkUp.Helpers;
using LinkUp.Models;
using LinkUp.Models.DTOs;
using LinkUp.Repository.Interface;
using LinkUp.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace LinkUp.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/user")]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly RideMatchContext _context;

        public UsersController(IUserService userService,
                               IMapper mapper,
                               RideMatchContext context)
        {
            _userService = userService;
            _mapper = mapper;
            _context = context;
        }

        // POST api/auth
        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Authenticate([FromBody]LoginDTO loginDto)
        {
            try
            {
                var users = _userService.Authenticate(username: loginDto.Username, password: loginDto.Password);

                if (users == null)
                    return BadRequest(new { message = "username or password is incorrect" });

                // return basic user info (without password) and token to store client side
                return Ok(new
                {
                    userId = users.UserId,
                    users.UserName,
                    users.Email,
                    users.PhoneNumber,
                    users.Role,
                    users.UserStatus,
                    token = users.Token,
                    users.ImageUrl
                });

            }
            catch (Exception)
            {
                return BadRequest();
            }


        }

        [AllowAnonymous]
        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody]UserDTO userDto)
        {
            // map dto to entity
            var user = _mapper.Map<Users>(userDto);

            try
            {
                // save 
                await _userService.Create(user, userDto.Password);
                return Ok();
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }
        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var users = await _userService.GetAll();
            var userDtos = _mapper.Map<IList<Users>>(users);
            return Ok(userDtos);
        }

        [AllowAnonymous]
        [HttpGet("{userId}")]
        public async Task<IActionResult> GetById(Guid userId)
        {
            Users user = await _userService.GetById(userId);
            if (user == null)
            {
                return NotFound("The user record couldn't be found.");
            }

            return Ok(user);
        }

        [AllowAnonymous]
        [HttpGet("email/{email}")]
        public async Task<IActionResult> GetByEmail(string email)
        {
            Users user = await _userService.GetByEmail(email);
            if (user == null)
            {
                return NotFound("The user record couldn't be found.");
            }

            return Ok(user);
        }


        [Authorize(Roles = "Admin, Driver, Rider")]
        [HttpPut("{userId}")]
        public async Task<IActionResult> Update(Guid userId, UserDTO userDto)
        {
            // map dto to entity and set id
            var user = _mapper.Map<Users>(userDto);
            user.UserId = userId;

            try
            {
                // save 
                await _userService.Update(user, userDto.Password);
                return Ok();
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpPatch("status/{userId}")]
        public async Task Patch( Guid userId, [FromBody] JsonPatchDocument<UserStatusDTO> usersPatch)
        {
            Users user = await _userService.GetById(userId);
            UserStatusDTO userStatusDTO = _mapper.Map<UserStatusDTO>(user);
            usersPatch.ApplyTo(userStatusDTO);
            _mapper.Map(userStatusDTO, user);
            _context.Update(user);
            return;
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete("{userId}")]
        public IActionResult Delete(Guid userId)
        {
            _userService.Delete(userId);
            return Ok();
        }

    }
}
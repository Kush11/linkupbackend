﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinkUp.Context;
using LinkUp.Models;
using LinkUp.Repository.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LinkUp.Controllers
{
    [Authorize]
    [Route("api/Review")]
    [ApiController]
    public class AppReviewController : ControllerBase
    {
        private readonly _dataRepository<AppReview> _dataRepository;
        private readonly RideMatchContext _context;

        public AppReviewController(_dataRepository<AppReview> dataRepository, RideMatchContext context)
        {
            _dataRepository = dataRepository;
            _context = context;
        }


        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            IEnumerable<AppReview> reviews = await _dataRepository.GetAll();
            return Ok(reviews);

        }

        [AllowAnonymous]
        [HttpGet("{id}", Name = "GetReview")]
        public async Task<IActionResult> Get(Guid id)
        {
            AppReview review = await  _dataRepository.Get(id);
            if (review == null)
            {
                return NotFound("The review recored couldn't be found.");
            }
            return Ok(review);
        }




        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] AppReview review)
        {
            if (review == null)
            {
                return BadRequest("review is empty");
            }
            await _dataRepository.Add(review);
            return CreatedAtRoute(
                    "GetReview",
                    new { Id = review.ReviewId },
                    review);
        }



        [AllowAnonymous]
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, [FromBody] AppReview review)
        {
            if (review == null)
            {
                return BadRequest("review is null ");
            }

            AppReview reviewToUpdate = await _dataRepository.Get(id);
            if (reviewToUpdate == null)
            {
                return NotFound("The review record could not be found");
            }

            await _dataRepository.Update(reviewToUpdate, review);
            return NoContent();
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            await _dataRepository.Delete(id);
            return Ok();
        }

    }
}
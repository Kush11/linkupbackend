﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinkUp.Models;
using LinkUp.Repository.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;

namespace LinkUp.Controllers
{
    [Authorize]
    [Route("api/decline")]
    [ApiController]
    public class DeclineMessageController : ControllerBase
    {
        private readonly IHubContext<NotifyHub> _hubContext;

        public DeclineMessageController(IHubContext<NotifyHub> hubContext)
        {
            _hubContext = hubContext;
        }
        [Authorize(Roles = "Admin, Rider, Driver")]
        [HttpGet("{id}", Name = "receiveDeclineMessage")]
        public string NotifyClient(string id, string message)
        {
            _hubContext.Clients.User(id).SendAsync(message);
            return "message sent to: " + id;
        }


    }
}
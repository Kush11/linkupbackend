﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinkUp.Models;
using LinkUp.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace LinkUp.Controllers
{
    [Authorize]
    [Route("api/encrypt")]
    [ApiController]
    public class PaymentEncryptionController : ControllerBase
    {
        private readonly IPaymentDataEncryption _paymentEncryptionService;
        private readonly IConfiguration configuration;


        public PaymentEncryptionController(IPaymentDataEncryption paymentEncryptionService, IConfiguration iConfig)
        {
            _paymentEncryptionService = paymentEncryptionService;
            configuration = iConfig;

        }

        [Authorize(Roles = "Admin, Driver, Rider")]
        [HttpGet("getKey")]
        public IActionResult getEncryptKey()
        {
            try
            {
                string secretKey = configuration.GetValue<string>("RavePayment:RaveProdSecKey");

                var key = _paymentEncryptionService.GetEncryptionKey(secretKey);

                if (key == null)
                    return BadRequest(new { message = "Key is not present" });

                return Ok(new
                {
                   encryptionKey = key
                });

            }
            catch (Exception)
            {
                return BadRequest();
            }

        }

        [Authorize(Roles = "Admin, Driver, Rider")]
        [HttpGet("seckey")]
        public IActionResult getSecretKey()
        {
            try
            {
                string secretKey = configuration.GetValue<string>("RavePayment:RaveProdSecKey");
                if (secretKey == null)
                    return BadRequest(new { message = "Key is not present" });

                return Ok(new
                {
                    encryptionKey = secretKey
                });

            }
            catch (Exception)
            {
                return BadRequest();
            }

        }

        [Authorize(Roles = "Admin, Driver, Rider")]
        [HttpPost("{key}")]
        public IActionResult encryptPayment(string key, [FromBody] EncryptPaymentData encryptionData)
        {
            try
            {
                var encryptionKey = key;
                string payLoad = JsonConvert.SerializeObject(encryptionData);
                var encryptedPayload = _paymentEncryptionService.EncryptData(encryptionKey, payLoad);

                if (encryptedPayload == null)
                    return BadRequest(new { message = "Payment payload is not present" });

                return Ok(new
                {
                    encryptionData = encryptedPayload
                });

            }
            catch (Exception)
            {
                return BadRequest();
            }

        }

        [Authorize(Roles = "Admin, Driver, Rider")]
        [HttpPost("decryptPayload/{key}")]
        public IActionResult decryptPayment(string key, [FromBody]EncryptedData encryptedData)
        {
            try
            {
                var encryptionKey = key;
                var encryptedPaymentData = encryptedData.PaymentData;
                var decryptedPayload = _paymentEncryptionService.DecryptData(encryptedPaymentData.ToString(), encryptionKey.ToString());

                if (decryptedPayload == null)
                    return BadRequest(new { message = "Payment payload is not present" });

                return Ok(new
                {
                    decryptedData = decryptedPayload
                });

            }
            catch (Exception)
            {
                return BadRequest();
            }

        }

    }
}
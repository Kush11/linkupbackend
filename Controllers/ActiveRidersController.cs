﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using LinkUp.Context;
using LinkUp.Models;
using LinkUp.Models.DTOs;
using LinkUp.Repository.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LinkUp.Controllers
{
    [Authorize]
    [Route("api/ActiveRiders")]
    [ApiController]
    public class ActiveRidersController : ControllerBase
    {
        private readonly _dataRepository<ActiveRiders> _dataRepository;
        //private readonly RideMatchContext _context;
        private readonly IMapper _mapper;
        public ActiveRidersController(_dataRepository<ActiveRiders> dataRepository, IMapper mapper)
        {
            _dataRepository = dataRepository;
            //_context = context;
            _mapper = mapper;
        }

        // GET: api/ActiveRiders

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            IEnumerable<ActiveRiders> trips = await _dataRepository.GetAll();
            return Ok(trips);

        }

        // GET api/ActiveRiders/5
        [Authorize(Roles = "Admin, Rider, Driver")]
        [HttpGet("{id}", Name = "GetActiveRider")]
        public async Task<IActionResult> Get(Guid id)
        {
            ActiveRiders trips = await _dataRepository.Get(id);
            if (trips == null)
            {
                return NotFound("The trip record couldn't be found.");
            }
            return Ok(trips);
        }
        //[AllowAnonymous]
        //[Route("users")]
        //[HttpGet]
        //public IActionResult GetStore([FromQuery] Guid id)
        //{
        //    IQueryable<Stores> stores = _storesContext.Stores
        //        .Where(u => u.UserId == id);
        //    if (stores == null)
        //    {
        //        return NotFound("The store record couldn't be found");
        //    }
        //    return Ok(stores);
        //}



        [Authorize(Roles = "Admin, Rider")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ActiveRiders trips)
        {
            if (trips == null)
            {
                return BadRequest("trip request is null");
            }
            await _dataRepository.Add(trips);
            return CreatedAtRoute(
                    "GetActiveRider",
                    new { Id = trips.ActiveRiderId },
                    trips);
        }

        //Post: api/stores/ send-email
        //[Route("send-email")]
        //public async Task<IActionResult> SendEmailAsync([FromQuery] string username, string subject, string message)
        //{
        //    await _emailService.SendEmailAsync(username, subject, message);
        //    return Ok();
        //}

        //PUT: api/AvtiveRider/5
        [Authorize(Roles = "Admin, Rider, Driver")]
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, RiderDTO riderDto)
        {
            var trips = _mapper.Map<ActiveRiders>(riderDto);

            if (trips == null)
            {
                return BadRequest("trips is null ");
            }

            ActiveRiders tripToUpdate = await _dataRepository.Get(id);
            if (tripToUpdate == null)
            {
                return NotFound("The trip could not be found");
            }

            await _dataRepository.Update(tripToUpdate, trips);
            return NoContent();
        }

        //Delete: api/stores/5
        [Authorize(Roles = "Admin, Rider, Driver")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            await _dataRepository.Delete(id);
            return Ok();
        }
    }
}
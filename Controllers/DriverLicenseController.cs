﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinkUp.Context;
using LinkUp.Models;
using LinkUp.Repository.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LinkUp.Controllers
{
    [Route("api/license")]
    [ApiController]
    public class DriverLicenseController : ControllerBase
    {
        private readonly _imageRepository<DriverLicense> _dataRepository;

        public DriverLicenseController(_imageRepository<DriverLicense> dataRepository)
        {
            _dataRepository = dataRepository;
        }

        [Authorize(Roles = "Admin, Driver, Rider")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]DriverLicense filesData)
        {
            if (filesData == null) return BadRequest("Null File");

            await _dataRepository.Add(filesData);
            return CreatedAtRoute(
                 "GetLicense",
                    new { Id = filesData.LicenseId },
                    filesData);
        }

        [Authorize(Roles = "Admin, Driver, Rider")]
        [HttpGet]
        public async Task<IActionResult> Getimage()
        {

            IEnumerable<DriverLicense> files = await _dataRepository.GetAll();
            if (files == null)
            {
                return NotFound("The image couldn't be found");
            }
            return Ok(files);

        }

        [Authorize(Roles = "Admin, Driver, Rider")]
        [HttpGet("{id}", Name = "GetLicense")]
        public async Task<IActionResult> Get(Guid id)
        {
            DriverLicense images = await _dataRepository.Get(id);
            if (images == null)
            {
                return NotFound("The image couldn't be found.");
            }
            return Ok(images);
        }

        [Authorize(Roles = "Admin, Driver, Rider")]
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, [FromBody] DriverLicense image)
        {
            if (image == null)
            {
                return BadRequest("image is null ");
            }

            DriverLicense imageToUpdate = await _dataRepository.Get(id);
            if (imageToUpdate == null)
            {
                return NotFound("The image could not be found");
            }

            await _dataRepository.Update(imageToUpdate, image);
            return NoContent();
        }
    }
}
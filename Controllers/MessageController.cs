﻿using System;

using LinkUp.Models;
using LinkUp.Repository.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;

namespace LinkUp.Controllers
{
    [Authorize]
    [Route("api/message")]
    [ApiController]
    public class MessageController : ControllerBase
    {
        private readonly IHubContext<NotifyHub, ITypedHubClient> _hubContext;

        public MessageController(IHubContext<NotifyHub, ITypedHubClient> hubContext)
        {
            _hubContext = hubContext;
        }
        [Authorize(Roles = "Admin, Rider, Driver")]
        [HttpGet("{id}", Name="recieveMessage")]
        public string NotifyClient(string id, string message)
        {
            _hubContext.Clients.Client(id).ReceiveMessage(message);
            return "message sent to: " + id;
        }

       
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using LinkUp.Models;
using LinkUp.Repository.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LinkUp.Controllers
{
    [Route("api/location")]
    [ApiController]
    public class PickUpController : ControllerBase
    {
        private readonly _dataRepository<TripPickUp> _dataRepository;
        //private readonly RideMatchContext _context;
        public PickUpController(_dataRepository<TripPickUp> dataRepository)
        {
            _dataRepository = dataRepository;
            //_context = context;
        }

        // GET: api/location

        [Authorize(Roles = "Admin, Rider, Driver")]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            IEnumerable<TripPickUp> trips = await _dataRepository.GetAll();
            return Ok(trips);

        }

        // GET api/location/5
        [AllowAnonymous]
        [HttpGet("{id}", Name = "GetTripPickup")]
        public async Task<IActionResult> Get(Guid id)
        {
            TripPickUp trips = await _dataRepository.Get(id);
            if (trips == null)
            {
                return NotFound("The location could not be found.");
            }
            return Ok(trips);
        }


        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] TripPickUp trips)
        {
            if (trips == null)
            {
                return BadRequest("location request is null");
            }
            await _dataRepository.Add(trips);
            return CreatedAtRoute(
                    "GetTripPickup",
                    new { Id = trips.PickupId },
                    trips);
        }



        //PUT: api/Pickup/5
        [AllowAnonymous]
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, [FromBody] TripPickUp trips)
        {
            if (trips == null)
            {
                return BadRequest("location is null ");
            }

            TripPickUp tripToUpdate = await _dataRepository.Get(id);
            if (tripToUpdate == null)
            {
                return NotFound("The trip could not be found");
            }

            await _dataRepository.Update(tripToUpdate, trips);
            return NoContent();
        }

        //Delete: api/stores/5
        [Authorize(Roles = "Admin")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            await _dataRepository.Delete(id);
            return Ok();
        }
    }
}
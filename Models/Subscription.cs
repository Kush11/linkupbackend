﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LinkUp.Models
{
    public class Subscription
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid SubcriptionId               { get; set; }

        public Guid? KeyId                      { get; set; }

        public string Endpoint                  { get; set; }

        [ForeignKey("KeyId")]
        public virtual Keys keys                { get; set; }
    }

    public class PushNotificationServiceOptions
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid SubcriptionOptionId { get; set; }

        public string Subject           { get; set; }

        public string PublicKey         { get; set; }

        public string PrivateKey        { get; set; }
    }

    public class Keys
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid KeyId               { get; set; }

        public string auth              { get; set; }

        public string p256dh            { get; set; }
    }

}

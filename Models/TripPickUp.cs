﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LinkUp.Models
{
    public class TripPickUp
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid PickupId                              { get; set; }
        public Guid UserId                                { get; set; }
        public string PickupLongitude                     { get; set; }
        public string PickupLatitude                      { get; set; }
        public string UserRole                            { get; set; }

    }
}

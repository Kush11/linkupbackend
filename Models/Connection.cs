﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LinkUp.Models
{
    public class Connection
    {
        public string ConnectionId                  { get; set; }
        public string UserAgent                     { get; set; }
        public bool Connected                       { get; set; }

    }
}

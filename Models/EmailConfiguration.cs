﻿using LinkUp.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LinkUp.Models
{

    //public class IEmailConfiguration
    //{
    //    string SmtpServer { get; }
    //    int SmtpPort { get; }
    //    string SmtpUsername { get; set; }
    //    string SmtpPassword { get; set; }


    //    string PopServer { get; }
    //    int popPort { get; }
    //    string PopUsername { get; }
    //    string PopPassword { get; }
    //}


    public class EmailConfiguration
    {
        public string SmtpServer { get; set; }
        public int SmtpPort { get; set; }
        public string SmtpUsername { get; set; }
        public string SmtpPassword { get; set; }

        public string PopServer { get; set; }
        public int PopPort { get; set;  }
        public string PopUsername { get; set; }

        public string PopPassword { get; set; }
    }
}

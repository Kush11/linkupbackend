﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LinkUp.Models
{
    public class PushNotificationToken
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid TokenId              { get; set; }
        public Guid UserId               { get; set; }
        public string Token              { get; set; }
        [ForeignKey("UserId")]
        public virtual Users User        { get; set; }
    }
}

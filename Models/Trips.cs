﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LinkUp.Models
{
    public class Trips
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid TripId                                           { get; set; }
        public Guid? DriverDataId                                    { get; set; }
        public string TripConnectionId                               { get; set; }
        public int DriverTripStatus                                  { get; set; }  
        public string DriverStartLongitude                           { get; set; }
        public string DriverStartLatitude                            { get; set; }
        public string DriverEndLongitude                             { get; set; }
        public string DriverEndLatitude                              { get; set; }
        public int MaxRiderNumber                                    { get; set; }
        public int AllowedRiderCount                                 { get; set; }
        public string TripStartDateTime                              { get; set; }
        public string TripEndDateTime                                { get; set; }
        public string ActualTripStartDateTime                        { get; set; }
        public string ActualTripEndDateTime                          { get; set; }
        public string TripType                                       { get; set; }
        public string TripPickup                                     { get; set; }
        public string TripDestination                                { get; set; }
        public int AggregrateTripFee                                 { get; set; }
        public virtual ICollection<ActiveRiders> ActiveRiders        { get; set; } 
        [ForeignKey("DriverDataId")]
        public virtual DriverData TripDriver                         { get; set; }
    }
}

﻿using LinkUp.Models.DTOs;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LinkUp.Models
{
    public class DriverData
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid DriverDataId                              { get; set; }
        public Guid DriverId                                  { get; set; }
        public string CarType                                 { get; set; }
        public string PaymentAccountId                        { get; set; }
        public string WorkAddress                             { get; set; }
        public string LicenseUrl                              { get; set; }
        public string AccountNumber                           { get; set; }
        public string DriverBank                              { get; set; }
        public int DriverStatus                               { get; set; }
        public int Rating                                     { get; set; }
        public int RideDeclineCount                           { get; set; }
        public int DriverSeatCapacity                         { get; set; }
        [ForeignKey("DriverId")]
        public virtual Users Driver                           { get; set; }
        public int MaxCarSeatNumber                           { get; set; }
        public string PlateNumber                             { get; set; }
    }

    public class DriverLicense
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid LicenseId               { get; set; }
        public Guid DriverId                { get; set; }
        public byte[] CarLicense            { get; set; }
        [ForeignKey("DriverId")]
        public virtual Users Driver         { get; set; }

    }

}

﻿using LinkUp.Repository.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LinkUp.Models
{
    [Authorize]
    public class NotifyHub : Hub<ITypedHubClient>
    {
        //[Authorize(Roles = "Admin, Rider, Driver")]
        public async Task ReceiveMessage(string user, string message)
        {
            await Clients.User(user).ReceiveMessage(message);
        }

        public async Task ReceiveDeclineMessage(string user, string message)
        {
            await Clients.User(user).ReceiveDeclineMessage(message);
        }

        public string GetConnectionId()
        {
            return Context.ConnectionId;
        }

        public override async Task OnConnectedAsync()
        {

            Console.WriteLine("Who is connected: " + Context.ConnectionId);
            await base.OnConnectedAsync();

        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            await base.OnDisconnectedAsync(exception);
        }

        public async Task AddToGroup(string groupName)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, groupName);
        }

        public async Task SendGroupMessage(string groupName, string message)
        {
            await Clients.Group(groupName).SendGroupMessage(message);
        }


        public async Task ReceiveGroupMessage(string groupName, string message)
        {
            await Clients.Groups(groupName).ReceiveGroupMessage(message);
        }

        public async Task LeaveRoom(string groupName)
        {
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, groupName);
        }
    }


}

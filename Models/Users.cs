﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LinkUp.Models
{
    public class Users
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid UserId                         { get; set; }
        public string FirstName                    { get; set; }
        public string LastName                     { get; set; }
        public string UserName                     { get; set; }
        public string VerificationCode             { get; set; }
        public string PhoneNumber                  { get; set; }
        public string Role                         { get; set; }
        public int UserStatus                      { get; set; }
        public string Email                        { get; set; }
        public byte[] PasswordHash                 { get; set; }
        public byte[] PasswordSalt                 { get; set; }
        public string Password                     { get; set; }
        public string Token                        { get; set; }
        public string PaymentType                  { get; set; }
        public string SignupDate                   { get; set; }
        public string SignupTime                   { get; set; }
        public string ImageUrl                     { get; set; }
        public virtual Subscription Subscription   { get; set; }
        public ICollection<Connection> Connections { get; set; }
        public ICollection <UserPaymentData> UserPaymentData { get; set; }
        public ICollection<PushNotificationToken> PushNotificationTokens { get; set; }

    }
}

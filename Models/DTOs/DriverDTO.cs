﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LinkUp.Models.DTOs
{
    public class DriverDTO
    {

        public Guid DriverDataId { get; set; }
        public Guid DriverId { get; set; }
        public string CarType { get; set; }
        public string WorkAddress { get; set; }
        public byte[] CarDocument2 { get; set; }
        public int DriverStatus { get; set; }
        public int Rating { get; set; }
        public int RideDeclineCount { get; set; }
        public int DriverSeatCapacity { get; set; }
        public int MaxCarSeatNumber { get; set; }

    }
}

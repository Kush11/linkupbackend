﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LinkUp.Models.DTOs
{
    public class UserEmailDTO
    {
        public string Email { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LinkUp.Models.DTOs
{
    public class RiderDTO
    {
        public string TripStatus              { get; set; }
        public string PaymentStatus           { get; set; }
        public string RiderConnectId          { get; set; }

    }
}

﻿using Org.BouncyCastle.Bcpg.OpenPgp;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LinkUp.Models.DTOs
{
    public class UserDTO
    {
        
        public Guid UserId                { get; set; }
        public string Email               { get; set; }
        public string Username            { get; set; }
        public string LastName            { get; set; }
        public string PhoneNumber         { get; set; }
        public string Password            { get; set; }
        public string Token               { get; set; }
        public int UserStatus             { get; set; }
        public string Role                { get; set; }
        public string SignupDate          { get; set; }
        public string SignupTime          { get; set; }
        public string VerificationCode    { get; set; }
        public string ImageUrl            { get; set; }


    }
}

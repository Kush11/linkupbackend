﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LinkUp.Models.DTOs
{
    public class TripDTO
    {
        public Guid TripId { get; set; }
        public string TripPickup { get; set; }
        public string TripDestination { get; set; }
        public string TripConnectionId { get; set; }
        public string ActualTripStartDateTime { get; set; }

        public Guid? DriverDataId { get; set; }
        public int DriverTripStatus { get; set; }
        public string DriverStartLongitude { get; set; }
        public string DriverStartLatitude { get; set; }
        public string DriverEndLongitude { get; set; }
        public string DriverEndLatitude { get; set; }
        public int MaxRiderNumber { get; set; }
        public string TripStartDateTime { get; set; }
        public int AllowedRiderCount    { get; set; }
        public string TripType { get; set; }
        public int AggregrateTripFee { get; set; }
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LinkUp.Models.DTOs
{
    public class ActiveRiderRequestDTO
    {
        public Guid UserId                                       { get; set; }
        public Guid? TripId                                      { get; set; }
        //public Guid PickUpId                                   { get; set; }
        public string TripStatus                                 { get; set; }
        public string CurrentLocationLongitude                   { get; set; }
        public string CurrentLocationLatitude                    { get; set; }
        public string RiderDestinationLatitude                   { get; set; }
        public string RiderDestinationLongitude                  { get; set; }
        public string PaymentStatus                              { get; set; }
        public int TripFee                                       { get; set; }
        public int BookedSeat                                    { get; set; }
        public string PaymentType                                { get; set; }
    }
}

﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LinkUp.Models
{
    public class ActiveRiders
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ActiveRiderId                { get; set; }
        public Guid UserId                       { get; set; }
        public Guid? TripId                      { get; set; }
        public string RiderConnectId             { get; set; }
        public string TripStatus                 { get; set; }                               
        public string CurrentLocationLongitude   { get; set; }
        public string TripStartDateTime          { get; set; }
        public string TripEndDateTime            { get; set; }
        public string CurrentLocationLatitude    { get; set; }
        public string RiderDestinationLatitude   { get; set; }
        public string RiderDestinationLongitude  { get; set; }
        public int RateDriver                    { get; set; }
        public string PaymentStatus              { get; set; }
        public int TripFee                       { get; set; }
        public int BookedSeat                    { get; set; }
        public string PaymentType                { get; set; }

        [ForeignKey("UserId")]
        public virtual Users User                { get; set; }
        
        [ForeignKey("TripId")]
        public virtual  Trips Trip               { get; set; }
    }
}

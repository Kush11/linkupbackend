﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LinkUp.Models
{
    public class Roles
    {
        public const string Admin = "Admin";
        public const string Driver = "Driver";
        public const string Rider  = "Rider";
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LinkUp.Models
{

    public class NotificationMessage
    {
        public PushNotification Notification { get; set; }
        public Object Data                   { get; set; }
        public string To                     {get; set;}

    }
    public class PushNotification
    {
        public string Title                  { get; set; }
        public string Body                   { get; set; }
        public string ReceiverName           { get; set; }
        public string ClickAction            { get; set; }
        //public string Token                  { get; set; }

    }


}

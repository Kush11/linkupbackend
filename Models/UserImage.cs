﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LinkUp.Models
{
    public class UserImage
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ImageId                { get; set; }
        [Required]
        public byte[] Image                { get; set; }
        public Guid UserId                 { get; set; }
        [ForeignKey("UserId")]
        public virtual Users User          { get; set; }
    }
}

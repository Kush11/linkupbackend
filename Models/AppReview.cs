﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LinkUp.Models
{
    public class AppReview
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ReviewId                   { get; set; }
        public Guid UserId                     { get; set; }
        public string UserReview               { get; set; }
        public int UserRating                  { get; set; }
        public string reviewType               { get; set; }
        public string ReviewTime               { get; set; }
        [ForeignKey("UserId")]
        public virtual Users User              { get; set; }
    }
}

﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using AutoMapper;
using LinkUp.Context;
using LinkUp.Extension;
using LinkUp.Helpers;
using LinkUp.Models;
using LinkUp.Models.DTOs;
using LinkUp.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using LinkUp.Repository.Interface;
using LinkUp.Repository;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace LinkUp
{
  public class Startup
  {
    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
            services.AddDbContext<RideMatchContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            services.AddMvc().AddJsonOptions(options => {
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            });

            services.AddScoped<_dataRepository<Trips>, ActiveTripRepository>();
            services.AddScoped<_dataRepository<ActiveRiders>, ActiveRidersRepository>();
            services.AddScoped<_dataRepository<TripPickUp>, PickUpRepository>();
            services.AddScoped<_dataRepository<DriverData>, DriverDataRepository>();
            services.AddScoped<_dataRepository<AppReview>, ReviewRepository>();
            services.AddScoped<_dataRepository<UserPaymentData>, PaymentDataRepository>();
            services.AddScoped<_imageRepository<UserImage>, UserImageRepository>();
            services.AddScoped<_imageRepository<DriverLicense>, DriverLicenseRepository>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IPaymentDataEncryption, PaymentDataEncryptionService>();
            services.AddScoped<IPushSubscriptionStore<PushNotificationToken>, PushSubscriptionService>();



            services.AddSwaggerDocumentation();
            services.ConfigureCors();

            services.ConfigureIISIntegration();
 
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            //services.Configure<TwilioConfig>(Configuration.GetSection("TwilioConfig"));
            //services.AddAutoMapper();

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<UserDTO, Users>();
                cfg.CreateMap<LoginDTO, Users>();
                cfg.CreateMap<TripDTO, Trips>();
                cfg.CreateMap<RiderDTO, ActiveRiders>();
                cfg.CreateMap<ActiveRiderRequestDTO, ActiveRiders>();

            });

            IMapper mapper = config.CreateMapper();

            services.AddSingleton(mapper);
            services.AddSingleton<EmailConfiguration>(Configuration.GetSection("EmailConfiguration").Get<EmailConfiguration>());
            services.AddTransient<IEmailService, EmailService>();
            services.AddMemoryCache();
            services.AddSession();

            // configure strongly typed settings object
            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);

            var vapidConfig = Configuration.GetSection("VapidConfig");
            services.Configure<VapidConfig>(vapidConfig);


            // configure jwt authentication
            var appSettings = appSettingsSection.Get<AppSettings>();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options =>
            {
                options.RequireHttpsMetadata = false;
                options.SaveToken = true;
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
                options.Events = new JwtBearerEvents
                {
                    OnMessageReceived = context =>
                    {
                        var accessToken = context.Request.Query["access_token"];


                        //if request is for hub...
                        var path = context.HttpContext.Request.Path;
                        if (!String.IsNullOrEmpty(accessToken) && (path.StartsWithSegments("/notifyHub")))
                        {
                            // Read the token out of the query string
                            context.Token = accessToken;

                        }
                        return Task.CompletedTask;

                    }
                };
                }
            );
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddSignalR();
            services.AddSingleton<IUserIdProvider, UserIdProvider>();


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
    {
      if (env.IsDevelopment())
      {
       
        app.UseDeveloperExceptionPage();
      }
      else
      {
                app.Use(async (context, next) =>
                {
                    await next();
                    if (context.Response.StatusCode == 404 && !Path.HasExtension(context.Request.Path.Value))
                    {
                        context.Request.Path = "/index.html";
                        await next();
                    }
                });
                app.UseHsts();
      }

        app.UseHttpsRedirection();

        //app.Use(async (context, next) => await AuthQueryStringToHeader(context, next));
        
        app.UseCors("CorsPolicy");
        app.UseAuthentication();
        app.UseSignalR(routes =>
        {
            routes.MapHub<NotifyHub>("/notifyHub");
        });
        app.UseStaticFiles();
        app.UseSession();
        app.UseMvc();
        app.UseSwaggerDocumentation();
            
        }
  }
}

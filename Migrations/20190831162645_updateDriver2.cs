﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LinkUp.Migrations
{
    public partial class updateDriver2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DriverData_Users_UserId",
                table: "DriverData");

            migrationBuilder.DropColumn(
                name: "DriverId",
                table: "Trips");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "DriverData",
                newName: "DriverId");

            migrationBuilder.RenameIndex(
                name: "IX_DriverData_UserId",
                table: "DriverData",
                newName: "IX_DriverData_DriverId");

            migrationBuilder.AddColumn<int>(
                name: "Rating",
                table: "DriverData",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddForeignKey(
                name: "FK_DriverData_Users_DriverId",
                table: "DriverData",
                column: "DriverId",
                principalTable: "Users",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DriverData_Users_DriverId",
                table: "DriverData");

            migrationBuilder.DropColumn(
                name: "Rating",
                table: "DriverData");

            migrationBuilder.RenameColumn(
                name: "DriverId",
                table: "DriverData",
                newName: "UserId");

            migrationBuilder.RenameIndex(
                name: "IX_DriverData_DriverId",
                table: "DriverData",
                newName: "IX_DriverData_UserId");

            migrationBuilder.AddColumn<Guid>(
                name: "DriverId",
                table: "Trips",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_DriverData_Users_UserId",
                table: "DriverData",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LinkUp.Migrations
{
    public partial class UpdateSubKeys : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Keys",
                table: "Subscriptions");

            migrationBuilder.AddColumn<Guid>(
                name: "KeyId",
                table: "Subscriptions",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Keys",
                columns: table => new
                {
                    KeyId = table.Column<Guid>(nullable: false),
                    auth = table.Column<string>(nullable: true),
                    p256dh = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Keys", x => x.KeyId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Subscriptions_KeyId",
                table: "Subscriptions",
                column: "KeyId");

            migrationBuilder.AddForeignKey(
                name: "FK_Subscriptions_Keys_KeyId",
                table: "Subscriptions",
                column: "KeyId",
                principalTable: "Keys",
                principalColumn: "KeyId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Subscriptions_Keys_KeyId",
                table: "Subscriptions");

            migrationBuilder.DropTable(
                name: "Keys");

            migrationBuilder.DropIndex(
                name: "IX_Subscriptions_KeyId",
                table: "Subscriptions");

            migrationBuilder.DropColumn(
                name: "KeyId",
                table: "Subscriptions");

            migrationBuilder.AddColumn<string>(
                name: "Keys",
                table: "Subscriptions",
                nullable: true);
        }
    }
}

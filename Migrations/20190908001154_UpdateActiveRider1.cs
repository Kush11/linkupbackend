﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LinkUp.Migrations
{
    public partial class UpdateActiveRider1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AggregrateTripFee",
                table: "Trips",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<string>(
                name: "PaymentStatus",
                table: "ActiveRiders",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "BookedSeat",
                table: "ActiveRiders",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "PaymentType",
                table: "ActiveRiders",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TripFee",
                table: "ActiveRiders",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AggregrateTripFee",
                table: "Trips");

            migrationBuilder.DropColumn(
                name: "BookedSeat",
                table: "ActiveRiders");

            migrationBuilder.DropColumn(
                name: "PaymentType",
                table: "ActiveRiders");

            migrationBuilder.DropColumn(
                name: "TripFee",
                table: "ActiveRiders");

            migrationBuilder.AlterColumn<int>(
                name: "PaymentStatus",
                table: "ActiveRiders",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LinkUp.Migrations
{
    public partial class UpdateTrips5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "RiderDestinationLongitutde",
                table: "ActiveRiders",
                newName: "RiderDestinationLongitude");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "RiderDestinationLongitude",
                table: "ActiveRiders",
                newName: "RiderDestinationLongitutde");
        }
    }
}

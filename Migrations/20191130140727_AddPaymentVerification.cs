﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LinkUp.Migrations
{
    public partial class AddPaymentVerification : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "PaymentType",
                table: "UserPaymentData",
                newName: "PaymentToken");

            migrationBuilder.AddColumn<Guid>(
                name: "PaymentId",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "UserId",
                table: "UserPaymentData",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<string>(
                name: "ActualTripEndDateTime",
                table: "Trips",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ActualTripStartDateTime",
                table: "Trips",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PaymentAccountId",
                table: "DriverData",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Users_PaymentId",
                table: "Users",
                column: "PaymentId");

            migrationBuilder.CreateIndex(
                name: "IX_UserPaymentData_UserId",
                table: "UserPaymentData",
                column: "UserId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_UserPaymentData_Users_UserId",
                table: "UserPaymentData",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Users_UserPaymentData_PaymentId",
                table: "Users",
                column: "PaymentId",
                principalTable: "UserPaymentData",
                principalColumn: "PaymentId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserPaymentData_Users_UserId",
                table: "UserPaymentData");

            migrationBuilder.DropForeignKey(
                name: "FK_Users_UserPaymentData_PaymentId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Users_PaymentId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_UserPaymentData_UserId",
                table: "UserPaymentData");

            migrationBuilder.DropColumn(
                name: "PaymentId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "UserPaymentData");

            migrationBuilder.DropColumn(
                name: "ActualTripEndDateTime",
                table: "Trips");

            migrationBuilder.DropColumn(
                name: "ActualTripStartDateTime",
                table: "Trips");

            migrationBuilder.DropColumn(
                name: "PaymentAccountId",
                table: "DriverData");

            migrationBuilder.RenameColumn(
                name: "PaymentToken",
                table: "UserPaymentData",
                newName: "PaymentType");
        }
    }
}

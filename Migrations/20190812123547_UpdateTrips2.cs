﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LinkUp.Migrations
{
    public partial class UpdateTrips2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ActiveRiders_TripPickUp_PickUpId",
                table: "ActiveRiders");

            migrationBuilder.DropForeignKey(
                name: "FK_TripPickUp_Trips_TripsTripId",
                table: "TripPickUp");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TripPickUp",
                table: "TripPickUp");

            migrationBuilder.DropIndex(
                name: "IX_TripPickUp_TripsTripId",
                table: "TripPickUp");

            migrationBuilder.DropColumn(
                name: "PickupDateTime",
                table: "TripPickUp");

            migrationBuilder.DropColumn(
                name: "TripsTripId",
                table: "TripPickUp");

            migrationBuilder.RenameTable(
                name: "TripPickUp",
                newName: "TripPickUps");

            migrationBuilder.AddColumn<string>(
                name: "DepartureDateTime",
                table: "Trips",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TripEndDateTime",
                table: "Trips",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "TripPickUpPickupId",
                table: "Trips",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TripStartDateTime",
                table: "Trips",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TripEndDateTime",
                table: "ActiveRiders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TripStartDateTime",
                table: "ActiveRiders",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_TripPickUps",
                table: "TripPickUps",
                column: "PickupId");

            migrationBuilder.CreateTable(
                name: "UserPaymentData",
                columns: table => new
                {
                    PaymentId = table.Column<Guid>(nullable: false),
                    PaymentType = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserPaymentData", x => x.PaymentId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Trips_TripPickUpPickupId",
                table: "Trips",
                column: "TripPickUpPickupId");

            migrationBuilder.AddForeignKey(
                name: "FK_ActiveRiders_TripPickUps_PickUpId",
                table: "ActiveRiders",
                column: "PickUpId",
                principalTable: "TripPickUps",
                principalColumn: "PickupId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Trips_TripPickUps_TripPickUpPickupId",
                table: "Trips",
                column: "TripPickUpPickupId",
                principalTable: "TripPickUps",
                principalColumn: "PickupId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ActiveRiders_TripPickUps_PickUpId",
                table: "ActiveRiders");

            migrationBuilder.DropForeignKey(
                name: "FK_Trips_TripPickUps_TripPickUpPickupId",
                table: "Trips");

            migrationBuilder.DropTable(
                name: "UserPaymentData");

            migrationBuilder.DropIndex(
                name: "IX_Trips_TripPickUpPickupId",
                table: "Trips");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TripPickUps",
                table: "TripPickUps");

            migrationBuilder.DropColumn(
                name: "DepartureDateTime",
                table: "Trips");

            migrationBuilder.DropColumn(
                name: "TripEndDateTime",
                table: "Trips");

            migrationBuilder.DropColumn(
                name: "TripPickUpPickupId",
                table: "Trips");

            migrationBuilder.DropColumn(
                name: "TripStartDateTime",
                table: "Trips");

            migrationBuilder.DropColumn(
                name: "TripEndDateTime",
                table: "ActiveRiders");

            migrationBuilder.DropColumn(
                name: "TripStartDateTime",
                table: "ActiveRiders");

            migrationBuilder.RenameTable(
                name: "TripPickUps",
                newName: "TripPickUp");

            migrationBuilder.AddColumn<string>(
                name: "PickupDateTime",
                table: "TripPickUp",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "TripsTripId",
                table: "TripPickUp",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_TripPickUp",
                table: "TripPickUp",
                column: "PickupId");

            migrationBuilder.CreateIndex(
                name: "IX_TripPickUp_TripsTripId",
                table: "TripPickUp",
                column: "TripsTripId");

            migrationBuilder.AddForeignKey(
                name: "FK_ActiveRiders_TripPickUp_PickUpId",
                table: "ActiveRiders",
                column: "PickUpId",
                principalTable: "TripPickUp",
                principalColumn: "PickupId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TripPickUp_Trips_TripsTripId",
                table: "TripPickUp",
                column: "TripsTripId",
                principalTable: "Trips",
                principalColumn: "TripId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

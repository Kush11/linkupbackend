﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LinkUp.Migrations
{
    public partial class UpdateConnectionId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "driverConnectId",
                table: "DriverData");

            migrationBuilder.RenameColumn(
                name: "riderConnectId",
                table: "ActiveRiders",
                newName: "RiderConnectId");

            migrationBuilder.AddColumn<string>(
                name: "TripConnectionId",
                table: "Trips",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TripConnectionId",
                table: "Trips");

            migrationBuilder.RenameColumn(
                name: "RiderConnectId",
                table: "ActiveRiders",
                newName: "riderConnectId");

            migrationBuilder.AddColumn<string>(
                name: "driverConnectId",
                table: "DriverData",
                nullable: true);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LinkUp.Migrations
{
    public partial class AddDriverLicense1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_License_DriverData_DriverId",
                table: "License");

            migrationBuilder.AddForeignKey(
                name: "FK_License_Users_DriverId",
                table: "License",
                column: "DriverId",
                principalTable: "Users",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_License_Users_DriverId",
                table: "License");

            migrationBuilder.AddForeignKey(
                name: "FK_License_DriverData_DriverId",
                table: "License",
                column: "DriverId",
                principalTable: "DriverData",
                principalColumn: "DriverDataId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

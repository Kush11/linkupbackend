﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LinkUp.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Trips",
                columns: table => new
                {
                    TripId = table.Column<Guid>(nullable: false),
                    DriverId = table.Column<Guid>(nullable: false),
                    TripStatus = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trips", x => x.TripId);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    FirstName = table.Column<string>(nullable: false),
                    LastName = table.Column<string>(nullable: false),
                    UserName = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: false),
                    UserStatus = table.Column<int>(nullable: false),
                    Email = table.Column<string>(nullable: true),
                    CarType = table.Column<string>(nullable: true),
                    WorkAddress = table.Column<string>(nullable: true),
                    CarLicense = table.Column<byte[]>(nullable: true),
                    CarDocument1 = table.Column<byte[]>(nullable: true),
                    CarDocument2 = table.Column<byte[]>(nullable: true),
                    CarSeatNumber = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.UserId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Trips");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LinkUp.Migrations
{
    public partial class UpdateActiveRiders : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ActiveRiders_TripPickUp_TripPickUpPickupId",
                table: "ActiveRiders");

            migrationBuilder.DropIndex(
                name: "IX_ActiveRiders_TripPickUpPickupId",
                table: "ActiveRiders");

            migrationBuilder.DropColumn(
                name: "PaymentType",
                table: "ActiveRiders");

            migrationBuilder.DropColumn(
                name: "TripPickUpPickupId",
                table: "ActiveRiders");

            migrationBuilder.RenameColumn(
                name: "RiderId",
                table: "ActiveRiders",
                newName: "UserId");

            migrationBuilder.RenameColumn(
                name: "ActiveRideId",
                table: "ActiveRiders",
                newName: "ActiveRiderId");

            migrationBuilder.AddColumn<string>(
                name: "PaymentType",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "PickUpId",
                table: "ActiveRiders",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_ActiveRiders_PickUpId",
                table: "ActiveRiders",
                column: "PickUpId");

            migrationBuilder.CreateIndex(
                name: "IX_ActiveRiders_UserId",
                table: "ActiveRiders",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_ActiveRiders_TripPickUp_PickUpId",
                table: "ActiveRiders",
                column: "PickUpId",
                principalTable: "TripPickUp",
                principalColumn: "PickupId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ActiveRiders_Users_UserId",
                table: "ActiveRiders",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ActiveRiders_TripPickUp_PickUpId",
                table: "ActiveRiders");

            migrationBuilder.DropForeignKey(
                name: "FK_ActiveRiders_Users_UserId",
                table: "ActiveRiders");

            migrationBuilder.DropIndex(
                name: "IX_ActiveRiders_PickUpId",
                table: "ActiveRiders");

            migrationBuilder.DropIndex(
                name: "IX_ActiveRiders_UserId",
                table: "ActiveRiders");

            migrationBuilder.DropColumn(
                name: "PaymentType",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "PickUpId",
                table: "ActiveRiders");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "ActiveRiders",
                newName: "RiderId");

            migrationBuilder.RenameColumn(
                name: "ActiveRiderId",
                table: "ActiveRiders",
                newName: "ActiveRideId");

            migrationBuilder.AddColumn<string>(
                name: "PaymentType",
                table: "ActiveRiders",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "TripPickUpPickupId",
                table: "ActiveRiders",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ActiveRiders_TripPickUpPickupId",
                table: "ActiveRiders",
                column: "TripPickUpPickupId");

            migrationBuilder.AddForeignKey(
                name: "FK_ActiveRiders_TripPickUp_TripPickUpPickupId",
                table: "ActiveRiders",
                column: "TripPickUpPickupId",
                principalTable: "TripPickUp",
                principalColumn: "PickupId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

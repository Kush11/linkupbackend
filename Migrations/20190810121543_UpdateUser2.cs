﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LinkUp.Migrations
{
    public partial class UpdateUser2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CurrentLocation",
                table: "ActiveRiders");

            migrationBuilder.DropColumn(
                name: "RiderDestination",
                table: "ActiveRiders");

            migrationBuilder.RenameColumn(
                name: "CarSeatNumber",
                table: "Users",
                newName: "RideDeclineCount");

            migrationBuilder.AddColumn<string>(
                name: "FirstName",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LastName",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "AllowedRiderCount",
                table: "Trips",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "MaxRiderNumber",
                table: "Trips",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "CurrentLocationLatitude",
                table: "ActiveRiders",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "CurrentLocationLongitude",
                table: "ActiveRiders",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "RateDriver",
                table: "ActiveRiders",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "RiderDestinationLatitude",
                table: "ActiveRiders",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "RiderDestinationLongitutde",
                table: "ActiveRiders",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<Guid>(
                name: "TripPickUpPickupId",
                table: "ActiveRiders",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ActiveRiders_TripPickUpPickupId",
                table: "ActiveRiders",
                column: "TripPickUpPickupId");

            migrationBuilder.AddForeignKey(
                name: "FK_ActiveRiders_TripPickUp_TripPickUpPickupId",
                table: "ActiveRiders",
                column: "TripPickUpPickupId",
                principalTable: "TripPickUp",
                principalColumn: "PickupId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ActiveRiders_TripPickUp_TripPickUpPickupId",
                table: "ActiveRiders");

            migrationBuilder.DropIndex(
                name: "IX_ActiveRiders_TripPickUpPickupId",
                table: "ActiveRiders");

            migrationBuilder.DropColumn(
                name: "FirstName",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "LastName",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "AllowedRiderCount",
                table: "Trips");

            migrationBuilder.DropColumn(
                name: "MaxRiderNumber",
                table: "Trips");

            migrationBuilder.DropColumn(
                name: "CurrentLocationLatitude",
                table: "ActiveRiders");

            migrationBuilder.DropColumn(
                name: "CurrentLocationLongitude",
                table: "ActiveRiders");

            migrationBuilder.DropColumn(
                name: "RateDriver",
                table: "ActiveRiders");

            migrationBuilder.DropColumn(
                name: "RiderDestinationLatitude",
                table: "ActiveRiders");

            migrationBuilder.DropColumn(
                name: "RiderDestinationLongitutde",
                table: "ActiveRiders");

            migrationBuilder.DropColumn(
                name: "TripPickUpPickupId",
                table: "ActiveRiders");

            migrationBuilder.RenameColumn(
                name: "RideDeclineCount",
                table: "Users",
                newName: "CarSeatNumber");

            migrationBuilder.AddColumn<string>(
                name: "CurrentLocation",
                table: "ActiveRiders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RiderDestination",
                table: "ActiveRiders",
                nullable: true);
        }
    }
}

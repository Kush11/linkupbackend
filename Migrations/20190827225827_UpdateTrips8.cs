﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LinkUp.Migrations
{
    public partial class UpdateTrips8 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Trips_DriverData_DriverId",
                table: "Trips");

            migrationBuilder.DropIndex(
                name: "IX_Trips_DriverId",
                table: "Trips");

            migrationBuilder.CreateIndex(
                name: "IX_Trips_DriverDataId",
                table: "Trips",
                column: "DriverDataId");

            migrationBuilder.AddForeignKey(
                name: "FK_Trips_DriverData_DriverDataId",
                table: "Trips",
                column: "DriverDataId",
                principalTable: "DriverData",
                principalColumn: "DriverDataId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Trips_DriverData_DriverDataId",
                table: "Trips");

            migrationBuilder.DropIndex(
                name: "IX_Trips_DriverDataId",
                table: "Trips");

            migrationBuilder.CreateIndex(
                name: "IX_Trips_DriverId",
                table: "Trips",
                column: "DriverId");

            migrationBuilder.AddForeignKey(
                name: "FK_Trips_DriverData_DriverId",
                table: "Trips",
                column: "DriverId",
                principalTable: "DriverData",
                principalColumn: "DriverDataId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

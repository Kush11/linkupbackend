﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LinkUp.Migrations
{
    public partial class UpdateTrips4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ActiveRiders_Trips_TripId",
                table: "ActiveRiders");

            migrationBuilder.AlterColumn<Guid>(
                name: "TripId",
                table: "ActiveRiders",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AddForeignKey(
                name: "FK_ActiveRiders_Trips_TripId",
                table: "ActiveRiders",
                column: "TripId",
                principalTable: "Trips",
                principalColumn: "TripId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ActiveRiders_Trips_TripId",
                table: "ActiveRiders");

            migrationBuilder.AlterColumn<Guid>(
                name: "TripId",
                table: "ActiveRiders",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ActiveRiders_Trips_TripId",
                table: "ActiveRiders",
                column: "TripId",
                principalTable: "Trips",
                principalColumn: "TripId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

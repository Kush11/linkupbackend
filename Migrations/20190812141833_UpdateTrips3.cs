﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LinkUp.Migrations
{
    public partial class UpdateTrips3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ActiveRiders_TripPickUps_PickUpId",
                table: "ActiveRiders");

            migrationBuilder.DropForeignKey(
                name: "FK_Trips_TripPickUps_TripPickUpPickupId",
                table: "Trips");

            migrationBuilder.DropIndex(
                name: "IX_Trips_TripPickUpPickupId",
                table: "Trips");

            migrationBuilder.DropIndex(
                name: "IX_ActiveRiders_PickUpId",
                table: "ActiveRiders");

            migrationBuilder.DropColumn(
                name: "TripPickUpPickupId",
                table: "Trips");

            migrationBuilder.DropColumn(
                name: "PickUpId",
                table: "ActiveRiders");

            migrationBuilder.AddColumn<Guid>(
                name: "PickUpId",
                table: "Trips",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<string>(
                name: "PickUpLatitude",
                table: "ActiveRiders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PickUpLongitude",
                table: "ActiveRiders",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "DriverData",
                columns: table => new
                {
                    DriverId = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    CarType = table.Column<string>(nullable: true),
                    WorkAddress = table.Column<string>(nullable: true),
                    CarLicense = table.Column<byte[]>(nullable: true),
                    CarDocument1 = table.Column<byte[]>(nullable: true),
                    CarDocument2 = table.Column<byte[]>(nullable: true),
                    RideDeclineCount = table.Column<int>(nullable: false),
                    DriverSeatCapacity = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DriverData", x => x.DriverId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Trips_DriverId",
                table: "Trips",
                column: "DriverId");

            migrationBuilder.CreateIndex(
                name: "IX_Trips_PickUpId",
                table: "Trips",
                column: "PickUpId");

            migrationBuilder.AddForeignKey(
                name: "FK_Trips_DriverData_DriverId",
                table: "Trips",
                column: "DriverId",
                principalTable: "DriverData",
                principalColumn: "DriverId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Trips_TripPickUps_PickUpId",
                table: "Trips",
                column: "PickUpId",
                principalTable: "TripPickUps",
                principalColumn: "PickupId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Trips_DriverData_DriverId",
                table: "Trips");

            migrationBuilder.DropForeignKey(
                name: "FK_Trips_TripPickUps_PickUpId",
                table: "Trips");

            migrationBuilder.DropTable(
                name: "DriverData");

            migrationBuilder.DropIndex(
                name: "IX_Trips_DriverId",
                table: "Trips");

            migrationBuilder.DropIndex(
                name: "IX_Trips_PickUpId",
                table: "Trips");

            migrationBuilder.DropColumn(
                name: "PickUpId",
                table: "Trips");

            migrationBuilder.DropColumn(
                name: "PickUpLatitude",
                table: "ActiveRiders");

            migrationBuilder.DropColumn(
                name: "PickUpLongitude",
                table: "ActiveRiders");

            migrationBuilder.AddColumn<Guid>(
                name: "TripPickUpPickupId",
                table: "Trips",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "PickUpId",
                table: "ActiveRiders",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_Trips_TripPickUpPickupId",
                table: "Trips",
                column: "TripPickUpPickupId");

            migrationBuilder.CreateIndex(
                name: "IX_ActiveRiders_PickUpId",
                table: "ActiveRiders",
                column: "PickUpId");

            migrationBuilder.AddForeignKey(
                name: "FK_ActiveRiders_TripPickUps_PickUpId",
                table: "ActiveRiders",
                column: "PickUpId",
                principalTable: "TripPickUps",
                principalColumn: "PickupId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Trips_TripPickUps_TripPickUpPickupId",
                table: "Trips",
                column: "TripPickUpPickupId",
                principalTable: "TripPickUps",
                principalColumn: "PickupId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

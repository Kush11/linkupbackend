﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LinkUp.Migrations
{
    public partial class UpdateSubKeys1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "SubscriptionSubcriptionId",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "reviewType",
                table: "AppReview",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Users_SubscriptionSubcriptionId",
                table: "Users",
                column: "SubscriptionSubcriptionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Subscriptions_SubscriptionSubcriptionId",
                table: "Users",
                column: "SubscriptionSubcriptionId",
                principalTable: "Subscriptions",
                principalColumn: "SubcriptionId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_Subscriptions_SubscriptionSubcriptionId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Users_SubscriptionSubcriptionId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "SubscriptionSubcriptionId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "reviewType",
                table: "AppReview");
        }
    }
}

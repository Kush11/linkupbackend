﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LinkUp.Migrations
{
    public partial class UpdateTrips6 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Trips_DriverData_DriverId",
                table: "Trips");

            migrationBuilder.DropForeignKey(
                name: "FK_Trips_TripPickUps_PickUpId",
                table: "Trips");

            migrationBuilder.AlterColumn<Guid>(
                name: "PickUpId",
                table: "Trips",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AlterColumn<Guid>(
                name: "DriverId",
                table: "Trips",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AddForeignKey(
                name: "FK_Trips_DriverData_DriverId",
                table: "Trips",
                column: "DriverId",
                principalTable: "DriverData",
                principalColumn: "DriverId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Trips_TripPickUps_PickUpId",
                table: "Trips",
                column: "PickUpId",
                principalTable: "TripPickUps",
                principalColumn: "PickupId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Trips_DriverData_DriverId",
                table: "Trips");

            migrationBuilder.DropForeignKey(
                name: "FK_Trips_TripPickUps_PickUpId",
                table: "Trips");

            migrationBuilder.AlterColumn<Guid>(
                name: "PickUpId",
                table: "Trips",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "DriverId",
                table: "Trips",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Trips_DriverData_DriverId",
                table: "Trips",
                column: "DriverId",
                principalTable: "DriverData",
                principalColumn: "DriverId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Trips_TripPickUps_PickUpId",
                table: "Trips",
                column: "PickUpId",
                principalTable: "TripPickUps",
                principalColumn: "PickupId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LinkUp.Migrations
{
    public partial class updateDriver : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_DriverData_UserId",
                table: "DriverData",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_DriverData_Users_UserId",
                table: "DriverData",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DriverData_Users_UserId",
                table: "DriverData");

            migrationBuilder.DropIndex(
                name: "IX_DriverData_UserId",
                table: "DriverData");
        }
    }
}

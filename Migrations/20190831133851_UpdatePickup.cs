﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LinkUp.Migrations
{
    public partial class UpdatePickup : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "TripId",
                table: "TripPickUps",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TripId",
                table: "TripPickUps");
        }
    }
}

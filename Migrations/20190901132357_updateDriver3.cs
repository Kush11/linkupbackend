﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LinkUp.Migrations
{
    public partial class updateDriver3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Trips_TripPickUps_PickUpId",
                table: "Trips");

            migrationBuilder.DropIndex(
                name: "IX_Trips_PickUpId",
                table: "Trips");

            migrationBuilder.DropColumn(
                name: "PickUpId",
                table: "Trips");

            migrationBuilder.AddColumn<string>(
                name: "TripPickup",
                table: "Trips",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TripPickup",
                table: "Trips");

            migrationBuilder.AddColumn<Guid>(
                name: "PickUpId",
                table: "Trips",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Trips_PickUpId",
                table: "Trips",
                column: "PickUpId");

            migrationBuilder.AddForeignKey(
                name: "FK_Trips_TripPickUps_PickUpId",
                table: "Trips",
                column: "PickUpId",
                principalTable: "TripPickUps",
                principalColumn: "PickupId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

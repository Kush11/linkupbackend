﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LinkUp.Migrations
{
    public partial class AddPushNotification3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PushNotificationToken_Users_UserId",
                table: "PushNotificationToken");

            migrationBuilder.AlterColumn<Guid>(
                name: "UserId",
                table: "PushNotificationToken",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AddForeignKey(
                name: "FK_PushNotificationToken_Users_UserId",
                table: "PushNotificationToken",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PushNotificationToken_Users_UserId",
                table: "PushNotificationToken");

            migrationBuilder.AlterColumn<Guid>(
                name: "UserId",
                table: "PushNotificationToken",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_PushNotificationToken_Users_UserId",
                table: "PushNotificationToken",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LinkUp.Migrations
{
    public partial class UpdateInitial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Password",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "PasswordHash",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "PasswordSalt",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Token",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserRole",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DriverEndLatitude",
                table: "Trips",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "DriverEndLongitude",
                table: "Trips",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "DriverStartLatitude",
                table: "Trips",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "DriverStartLongitude",
                table: "Trips",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "ActiveRiders",
                columns: table => new
                {
                    RiderId = table.Column<Guid>(nullable: false),
                    TripId = table.Column<Guid>(nullable: false),
                    RiderStatus = table.Column<string>(nullable: true),
                    CurrentLocation = table.Column<string>(nullable: true),
                    RiderDestination = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActiveRiders", x => x.RiderId);
                    table.ForeignKey(
                        name: "FK_ActiveRiders_Trips_TripId",
                        column: x => x.TripId,
                        principalTable: "Trips",
                        principalColumn: "TripId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TripPickUp",
                columns: table => new
                {
                    PickupId = table.Column<Guid>(nullable: false),
                    PickupLongitude = table.Column<int>(nullable: false),
                    PickupLatitude = table.Column<int>(nullable: false),
                    TripsTripId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TripPickUp", x => x.PickupId);
                    table.ForeignKey(
                        name: "FK_TripPickUp_Trips_TripsTripId",
                        column: x => x.TripsTripId,
                        principalTable: "Trips",
                        principalColumn: "TripId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ActiveRiders_TripId",
                table: "ActiveRiders",
                column: "TripId");

            migrationBuilder.CreateIndex(
                name: "IX_TripPickUp_TripsTripId",
                table: "TripPickUp",
                column: "TripsTripId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ActiveRiders");

            migrationBuilder.DropTable(
                name: "TripPickUp");

            migrationBuilder.DropColumn(
                name: "Password",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "PasswordHash",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "PasswordSalt",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Token",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "UserRole",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "DriverEndLatitude",
                table: "Trips");

            migrationBuilder.DropColumn(
                name: "DriverEndLongitude",
                table: "Trips");

            migrationBuilder.DropColumn(
                name: "DriverStartLatitude",
                table: "Trips");

            migrationBuilder.DropColumn(
                name: "DriverStartLongitude",
                table: "Trips");
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LinkUp.Migrations
{
    public partial class AddReview : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "SignupDate",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SignupTime",
                table: "Users",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "AppReview",
                columns: table => new
                {
                    ReviewId = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    UserReview = table.Column<string>(nullable: true),
                    UserRating = table.Column<int>(nullable: false),
                    ReviewTime = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppReview", x => x.ReviewId);
                    table.ForeignKey(
                        name: "FK_AppReview_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AppReview_UserId",
                table: "AppReview",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AppReview");

            migrationBuilder.DropColumn(
                name: "SignupDate",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "SignupTime",
                table: "Users");
        }
    }
}

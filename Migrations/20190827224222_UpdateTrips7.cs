﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LinkUp.Migrations
{
    public partial class UpdateTrips7 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "DriverId",
                table: "DriverData",
                newName: "DriverDataId");

            migrationBuilder.AddColumn<Guid>(
                name: "DriverDataId",
                table: "Trips",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DriverDataId",
                table: "Trips");

            migrationBuilder.RenameColumn(
                name: "DriverDataId",
                table: "DriverData",
                newName: "DriverId");
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LinkUp.Migrations
{
    public partial class AddDriverLicense : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CarDocument1",
                table: "DriverData");

            migrationBuilder.DropColumn(
                name: "CarLicense",
                table: "DriverData");

            migrationBuilder.AlterColumn<string>(
                name: "CarDocument2",
                table: "DriverData",
                nullable: true,
                oldClrType: typeof(byte[]),
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "License",
                columns: table => new
                {
                    LicenseId = table.Column<Guid>(nullable: false),
                    DriverId = table.Column<Guid>(nullable: false),
                    CarLicense = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_License", x => x.LicenseId);
                    table.ForeignKey(
                        name: "FK_License_DriverData_DriverId",
                        column: x => x.DriverId,
                        principalTable: "DriverData",
                        principalColumn: "DriverDataId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_License_DriverId",
                table: "License",
                column: "DriverId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "License");

            migrationBuilder.AlterColumn<byte[]>(
                name: "CarDocument2",
                table: "DriverData",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "CarDocument1",
                table: "DriverData",
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "CarLicense",
                table: "DriverData",
                nullable: true);
        }
    }
}

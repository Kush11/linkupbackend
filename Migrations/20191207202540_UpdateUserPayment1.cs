﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LinkUp.Migrations
{
    public partial class UpdateUserPayment1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_UserPaymentData_PaymentId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Users_PaymentId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_UserPaymentData_UserId",
                table: "UserPaymentData");

            migrationBuilder.DropColumn(
                name: "PaymentId",
                table: "Users");

            migrationBuilder.CreateIndex(
                name: "IX_UserPaymentData_UserId",
                table: "UserPaymentData",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_UserPaymentData_UserId",
                table: "UserPaymentData");

            migrationBuilder.AddColumn<Guid>(
                name: "PaymentId",
                table: "Users",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Users_PaymentId",
                table: "Users",
                column: "PaymentId");

            migrationBuilder.CreateIndex(
                name: "IX_UserPaymentData_UserId",
                table: "UserPaymentData",
                column: "UserId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Users_UserPaymentData_PaymentId",
                table: "Users",
                column: "PaymentId",
                principalTable: "UserPaymentData",
                principalColumn: "PaymentId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LinkUp.Migrations
{
    public partial class UpdateDriverAccountData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AccountNumber",
                table: "DriverData",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DriverBank",
                table: "DriverData",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AccountNumber",
                table: "DriverData");

            migrationBuilder.DropColumn(
                name: "DriverBank",
                table: "DriverData");
        }
    }
}

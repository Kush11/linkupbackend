﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LinkUp.Migrations
{
    public partial class AddPushNotification2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Subscriptions_Keys_KeyId",
                table: "Subscriptions");

            migrationBuilder.DropForeignKey(
                name: "FK_Users_Subscriptions_SubscriptionSubcriptionId",
                table: "Users");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Subscriptions",
                table: "Subscriptions");

            migrationBuilder.RenameTable(
                name: "Subscriptions",
                newName: "Subscription");

            migrationBuilder.RenameIndex(
                name: "IX_Subscriptions_KeyId",
                table: "Subscription",
                newName: "IX_Subscription_KeyId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Subscription",
                table: "Subscription",
                column: "SubcriptionId");

            migrationBuilder.CreateTable(
                name: "PushNotificationToken",
                columns: table => new
                {
                    TokenId = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    Token = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PushNotificationToken", x => x.TokenId);
                    table.ForeignKey(
                        name: "FK_PushNotificationToken_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PushNotificationToken_UserId",
                table: "PushNotificationToken",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Subscription_Keys_KeyId",
                table: "Subscription",
                column: "KeyId",
                principalTable: "Keys",
                principalColumn: "KeyId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Subscription_SubscriptionSubcriptionId",
                table: "Users",
                column: "SubscriptionSubcriptionId",
                principalTable: "Subscription",
                principalColumn: "SubcriptionId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Subscription_Keys_KeyId",
                table: "Subscription");

            migrationBuilder.DropForeignKey(
                name: "FK_Users_Subscription_SubscriptionSubcriptionId",
                table: "Users");

            migrationBuilder.DropTable(
                name: "PushNotificationToken");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Subscription",
                table: "Subscription");

            migrationBuilder.RenameTable(
                name: "Subscription",
                newName: "Subscriptions");

            migrationBuilder.RenameIndex(
                name: "IX_Subscription_KeyId",
                table: "Subscriptions",
                newName: "IX_Subscriptions_KeyId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Subscriptions",
                table: "Subscriptions",
                column: "SubcriptionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Subscriptions_Keys_KeyId",
                table: "Subscriptions",
                column: "KeyId",
                principalTable: "Keys",
                principalColumn: "KeyId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Subscriptions_SubscriptionSubcriptionId",
                table: "Users",
                column: "SubscriptionSubcriptionId",
                principalTable: "Subscriptions",
                principalColumn: "SubcriptionId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LinkUp.Migrations
{
    public partial class UpdateTrips : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_ActiveRiders",
                table: "ActiveRiders");

            migrationBuilder.DropColumn(
                name: "CarDocument1",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "CarDocument2",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "CarLicense",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "CarType",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "RideDeclineCount",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "WorkAddress",
                table: "Users");

            migrationBuilder.RenameColumn(
                name: "TripStatus",
                table: "Trips",
                newName: "DriverTripStatus");

            migrationBuilder.RenameColumn(
                name: "RiderStatus",
                table: "ActiveRiders",
                newName: "TripStatus");

            migrationBuilder.AddColumn<string>(
                name: "TripType",
                table: "Trips",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PickupDateTime",
                table: "TripPickUp",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ActiveRideId",
                table: "ActiveRiders",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<int>(
                name: "PaymentStatus",
                table: "ActiveRiders",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "PaymentType",
                table: "ActiveRiders",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_ActiveRiders",
                table: "ActiveRiders",
                column: "ActiveRideId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_ActiveRiders",
                table: "ActiveRiders");

            migrationBuilder.DropColumn(
                name: "TripType",
                table: "Trips");

            migrationBuilder.DropColumn(
                name: "PickupDateTime",
                table: "TripPickUp");

            migrationBuilder.DropColumn(
                name: "ActiveRideId",
                table: "ActiveRiders");

            migrationBuilder.DropColumn(
                name: "PaymentStatus",
                table: "ActiveRiders");

            migrationBuilder.DropColumn(
                name: "PaymentType",
                table: "ActiveRiders");

            migrationBuilder.RenameColumn(
                name: "DriverTripStatus",
                table: "Trips",
                newName: "TripStatus");

            migrationBuilder.RenameColumn(
                name: "TripStatus",
                table: "ActiveRiders",
                newName: "RiderStatus");

            migrationBuilder.AddColumn<byte[]>(
                name: "CarDocument1",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "CarDocument2",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "CarLicense",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CarType",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "RideDeclineCount",
                table: "Users",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "WorkAddress",
                table: "Users",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_ActiveRiders",
                table: "ActiveRiders",
                column: "RiderId");
        }
    }
}

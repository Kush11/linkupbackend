﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LinkUp.Migrations
{
    public partial class AddConnectionId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "driverConnectId",
                table: "DriverData",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "riderConnectId",
                table: "ActiveRiders",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "driverConnectId",
                table: "DriverData");

            migrationBuilder.DropColumn(
                name: "riderConnectId",
                table: "ActiveRiders");
        }
    }
}

﻿using LinkUp.Models;
using Microsoft.EntityFrameworkCore;



namespace LinkUp.Context
{
    public class RideMatchContext: DbContext
    {
        public RideMatchContext(DbContextOptions options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
        }
            public DbSet<Users> Users                           { get; set; }
            public DbSet<Trips> Trips                           { get; set; }
            public DbSet<UserPaymentData> PaymentData           { get; set; }
            public DbSet<ActiveRiders> ActiveRiders             { get; set; }
            public DbSet<TripPickUp> TripPickUps                { get; set; }
            public DbSet<DriverData> DriverData                 { get; set; }
            public DbSet<UserPaymentData> UserPaymentData       { get; set; }
            public DbSet<UserImage> Image                       { get; set; }
            public DbSet<DriverLicense> License                 { get; set; }

            public DbSet<AppReview> AppReview                   { get; set; }
            public DbSet<PushNotificationToken> PushNotificationToken { get; set; }



    }
}

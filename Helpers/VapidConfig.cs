﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LinkUp.Helpers
{
    public class VapidConfig
    {
        public string Subject     { get; set; }
        public string PublicKey   { get; set; }
        public string PrivateKey  { get; set; }
    }
}
